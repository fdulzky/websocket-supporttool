package at.florian.dulzky.supporttool.util;

import javax.faces.context.FacesContext;

public abstract class Helper {
	@SuppressWarnings("unchecked")
	public static <T> T findBean(String beanName) {
		FacesContext context = FacesContext.getCurrentInstance();
		return (T) context.getApplication().evaluateExpressionGet(context,
				"#{" + beanName + "}", Object.class);
	}
}
