package at.florian.dulzky.supporttool.util;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class Resources {

	@Produces
	@PersistenceContext
	private EntityManager em;

	@Produces
	public Logger produceLog(InjectionPoint injectionPoint) {
		Logger logger = Logger.getLogger(injectionPoint.getMember()
				.getDeclaringClass().getName());
		Handler handler = null;
		try {
			handler = new ConsoleHandler();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		logger.addHandler(handler);
		logger.setLevel(Level.FINEST);
		logger.log(Level.FINEST, "TEST ");
		return logger;
	}
}
