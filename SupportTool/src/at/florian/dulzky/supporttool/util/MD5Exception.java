package at.florian.dulzky.supporttool.util;


public class MD5Exception extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MD5Exception(String messString, Exception e) {
		super(messString, e);
	}
}
