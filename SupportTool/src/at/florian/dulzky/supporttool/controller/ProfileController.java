package at.florian.dulzky.supporttool.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;

import at.florian.dulzky.supporttool.model.User;
import at.florian.dulzky.supporttool.service.UserProfile;

@ManagedBean
public class ProfileController {

	@ManagedProperty(value = "#{loginController.user}")
	private User user;

	@Inject
	UserProfile profile;

	public String save() {
		try {
			this.user = profile.saveUser(this.user);
			return NavigationController.editProfile();
		} catch (Exception e) {
			return NavigationController.editProfile();
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
