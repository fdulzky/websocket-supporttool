package at.florian.dulzky.supporttool.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import at.florian.dulzky.supporttool.model.User;
import at.florian.dulzky.supporttool.service.UserLogin;

@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController {

	public static final String AUTH_KEY = "app.user.name";

	private User user;

	private UIComponent loginBtn;

	@PostConstruct
	public void init() {
		this.user = new User();
	}

	public User getUser() {
		return this.user;
	}

	@Inject
	private UserLogin login;

	public String goToIndex() {
		if (AUTH_KEY != null) {
			return "/backend/index";
		}
		return "/login";
	}

	public String doLogin() {
		this.user = login.checkLogin(user);
		if (this.user != null) {
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put(AUTH_KEY, this.user.getUsername());
			return "backend/index";
		}
		FacesMessage message = new FacesMessage(
				"Username oder Passwort sind falsch");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(loginBtn.getClientId(context), message);
		init();
		return "/login";
	}

	public String doLogout() {
		init();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put(AUTH_KEY, null);
		return "/login";
	}

	public UIComponent getLoginBtn() {
		return loginBtn;
	}

	public void setLoginBtn(UIComponent loginBtn) {
		this.loginBtn = loginBtn;
	}

}
