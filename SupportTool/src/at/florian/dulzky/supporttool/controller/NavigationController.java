package at.florian.dulzky.supporttool.controller;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "naviController")
public class NavigationController {

	public static String editProfile() {
		return "/backend/User/editProfile.jsf";
	}
}
