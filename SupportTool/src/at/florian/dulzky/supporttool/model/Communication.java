package at.florian.dulzky.supporttool.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "communication")
public class Communication {

	@Id
	private int id;

	@NotNull
	private String name;

	@OneToMany(mappedBy = "communication")
	private Collection<CommunicationText> text;
}
