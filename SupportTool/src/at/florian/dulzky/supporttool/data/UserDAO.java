package at.florian.dulzky.supporttool.data;

import at.florian.dulzky.supporttool.model.User;

public interface UserDAO extends DAOTemplate<User> {
	public User findUserByNameAndPasswort(String usrename, String password);
}
