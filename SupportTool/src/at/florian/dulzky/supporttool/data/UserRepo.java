package at.florian.dulzky.supporttool.data;

import java.util.List;

import javax.persistence.NoResultException;

import at.florian.dulzky.supporttool.model.User;

public class UserRepo extends Repository implements UserDAO {

	public User findUserByNameAndPasswort(String username, String password) {
		User user;
		try {
			user = (User) em
					.createQuery(
							"SELECT u FROM User u WHERE username = :username AND password = :password")
					.setParameter("username", username)
					.setParameter("password", MD5(password)).getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	@Override
	public User insert(User user) {
		em.persist(user);
		return user;
	}

	@Override
	public User update(User user) {
		try {
			tx.begin();
			em.merge(user);
			tx.commit();
		} catch (Exception e) {
			// TODO: Exception handling !!!
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public void delete(User user) {

	}

	@Override
	public User findById(int id) {

		return null;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
}
