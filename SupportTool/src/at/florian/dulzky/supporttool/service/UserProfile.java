package at.florian.dulzky.supporttool.service;

import javax.inject.Inject;

import at.florian.dulzky.supporttool.data.UserRepo;
import at.florian.dulzky.supporttool.model.User;

public class UserProfile {

	@Inject
	UserRepo userRepo;

	public User saveUser(User u) {
		return userRepo.update(u);
	}
}
