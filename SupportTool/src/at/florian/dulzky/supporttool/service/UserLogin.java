package at.florian.dulzky.supporttool.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import at.florian.dulzky.supporttool.data.UserRepo;
import at.florian.dulzky.supporttool.model.User;

@Stateless
public class UserLogin {

	@Inject
	UserRepo userRepo;

	public User checkLogin(User user) {

		User returnUser = userRepo.findUserByNameAndPasswort(
				user.getUsername(), user.getPassword());
		return returnUser;
	}
}
